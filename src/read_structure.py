'''
author: Wei Shi at 2022
version: 2022.4.7
function: structure data for gui
'''

import numpy as np
from ase.io import read
from ase import Atoms
from ase.build import surface,make_supercell
from pymatgen.io.cif import CifParser
from pymatgen.analysis.ewald import compute_average_oxidation_state
from pymatgen.io.ase import AseAtomsAdaptor as aaa


class Read:
    def __init__(self,structure):
        self.sname = structure
        self.parser = CifParser(self.sname)
        self.structure_list = self.parser.get_structures(primitive=False)
        self.py_structure = self.structure_list[0]
        self.charge_list = [compute_average_oxidation_state(site) for site in self.py_structure]
        self.structure = aaa.get_atoms(self.py_structure)
        self.structure.set_initial_charges(self.charge_list)
        self.s = self.structure

    def electrical_neutrality(self):
        charge=0
        for i in self.s:
            charge+=i.charge
        if charge != 0:
            neutrality = 'Non-neutral'
        else:
            neutrality = 'Neutral'
        return neutrality

    def structure_name(self):
        name = self.s.get_chemical_formula()
        return name 
    def structure_constants(self):
        constant_list = self.s.get_cell_lengths_and_angles ()
        constants = (' a = %d Å  alpha = %d degree'%(constant_list[0],constant_list[3]) +'\n' +
                    ' b = %d Å  beta = %d degree'%(constant_list[1],constant_list[4]) + '\n' +
                    ' c = %d Å  gamma = %d degree'%(constant_list[2],constant_list[5])
              )
        return( constants)

    def structure_reciprocal_lattice(self):
        reciprocal_list = self.s.get_reciprocal_cell()
        reciprocal_lattice =(' a* = ' + str(self.s.get_reciprocal_cell()[0])+'\n'+
              'b* = ' + str(self.s.get_reciprocal_cell()[1])+'\n'+
              'c* = ' + str(self.s.get_reciprocal_cell()[2])
              )
        return reciprocal_lattice
    def return_structure(self):
        return self.s
