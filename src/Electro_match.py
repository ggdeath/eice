'''
author: Wei Shi at 2022
version: 2022.9.18
function: Electrostatic matching
'''
from ase.io import read
from ase import Atoms
from pymatgen.io.cif import CifParser
from pymatgen.io.ase import AseAtomsAdaptor 
from ase.visualize import view
from pymatgen.core.interface import Interface
from Ewald_2D import Ewald2DSummation
from Ewald_3D import EwaldSummation
from pymatgen.io.cif import CifWriter
from pymatgen.core.surface import SlabGenerator
from pymatgen.analysis.ewald import compute_average_oxidation_state
from ase.build import surface
import copy

def trans_py_to_ase(py_structure):
    charge_list = [compute_average_oxidation_state(site) for site in py_structure]
    ase_structure = AseAtomsAdaptor.get_atoms (py_structure)
    ase_structure.set_initial_charges(charge_list)
    return ase_structure

def calc_energy(structure_list, energy_list,method):
    if method == 'Ewald_3D':
        for i in structure_list:
            col_energy = EwaldSummation(i).total_energy()
            energy_list.append(col_energy)
    else:
        for i in structure_list:
            col_energy = Ewald2DSummation(i).total_energy()
            energy_list.append(col_energy)
            
def trans_ase_to_py(structure):
    new_structure = AseAtomsAdaptor.get_structure(structure)
    charge_list = list(structure.get_initial_charges())
    new_structure.add_oxidation_state_by_site(charge_list)
    return new_structure

class Matching:
    def __init__(self, substrate_ori, film_ori, substrate_miller, film_miller, gap, substrate_layers, film_layers, method):
        self._substrate_ori = substrate_ori
        self._film_ori = film_ori
        self._substrate_miller = substrate_miller
        self._film_miller = film_miller
        self._gap = gap
        self._substrate_layers = substrate_layers
        self._film_layers = film_layers
        self._method = method

    ############# read substrate structure and surface #############
    def build_hetero(self):
        film_sl_vectors = []
        substrate_sl_vectors = []
        film_transformation = []
        substrate_transformation = []
        film_set = []
        substrate_set = []
        hetero_structure = []
        ase_substrate = []
        ase_film = []
        ase_hetero = []
        
        substrate_miller = []
        film_miller = []
        
        #### cut surface#########
        
        substrate_miller = substrate_miller
        substrate = surface(self._substrate_ori, self._substrate_miller, layers = self._substrate_layers, vacuum = 10)
        substrate = trans_ase_to_py(substrate)
        CifWriter(substrate).write_file('substrate.cif')

        film_miller = film_miller
        film = surface(self._film_ori, self._film_miller, layers = self._film_layers, vacuum = 10)
        film = trans_ase_to_py(film)
        CifWriter(film).write_file('film.cif')

        ########### Determine basic parameters of substrate and film ###############

        # basis vector of substrate and film (excluding c-axis)
        vec_set_film = np.array([film.lattice.matrix[0] , film.lattice.matrix[1]])
        vec_set_substrate = np.array([substrate.lattice.matrix[0] , substrate.lattice.matrix[1]])
        # matrix for substrates and films
        matrix_substrate_surface_v = np.mat (vec_set_substrate)
        matrix_substrate_surface_v = np.vstack([matrix_substrate_surface_v,[0,0,1]])
        matrix_film_surface_v = np.mat (vec_set_film)
        matrix_film_surface_v = np.vstack([matrix_film_surface_v,[0,0,1]])
        # area of substrate and film
        substrate_square = np.linalg.norm(np.cross(substrate.lattice.matrix[0],substrate.lattice.matrix[1]))
        film_square = np.linalg.norm(np.cross(film.lattice.matrix[0],film.lattice.matrix[1]))

        ################ Matching using zsl algorithm ######################### 
        
        G = zsl.ZSLGenerator (max_area_ratio_tol=0.3, max_area=500, max_length_tol=0.1, max_angle_tol=0.01) 
        transformation = G.generate_sl_transformation_sets (film_square , substrate_square)

        ################### Transformation matrix ############################ 

        for match in G.get_equiv_transformations(transformation, vec_set_film, vec_set_substrate):
            # film supercell
            film_sl_vectors.append(match[0])
            matrix1 = np.mat (match[0])
            matrix1 = np.vstack([matrix1,[0,0,1]])
            # substrate supercell
            substrate_sl_vectors.append(match[1])
            matrix2 = np.mat (match[1])
            matrix2 = np.vstack([matrix2,[0,0,1]])
            # transformation matrix
            film_trans = np.round(np.dot(matrix1,np.linalg.inv(matrix_film_surface_v)))
            # film_trans = supercell_trans_matrix(matrix_film_surface_v,matrix1)
            subs_trans = np.round(np.dot(matrix2,np.linalg.inv(matrix_substrate_surface_v)))
            # subs_trans = supercell_trans_matrix(matrix_substrate_surface_v,matrix2)
            film_transformation.append(film_trans)
            substrate_transformation.append(subs_trans)

        ##################### Build supercell #############################

        for i in film_transformation :
            copy1 = film.copy()
            copy1.make_supercell(i)
            film_set.append(copy1)
            temp1 = trans_py_to_ase (copy1)
            ase_film.append(temp1)
            
        for j in substrate_transformation :
            copy2 = substrate.copy()
            copy2.make_supercell(j)
            substrate_set.append(copy2)
            temp2 = trans_py_to_ase (copy2)
            ase_substrate.append(temp2)

        ####################### Build heterojunction ####################################

        #self._gap = float(input("\n Enter the interface spacing："))
        #print('\n*********Heterojunction is being built*********')
        for i,j in enumerate (ase_substrate):
            ase_film[i].set_cell(j.cell,scale_atoms=True)
            zmin = ase_film[i].positions[:, 2].min()
            zmax = j.positions[:, 2].max()
            ase_film[i].positions += (0, 0, zmax - zmin + self._gap) # Interface spacing
            interface = j + ase_film[i]
            interface.center(vacuum=10, axis=2)# vacuum
            ase_hetero.append(interface)
            py_structure = AseAtomsAdaptor.get_structure(interface)
            hetero_structure.append (py_structure)

        for i in range(len(film_transformation)):
            CifWriter(film_set[i]).write_file('film_sp_%d.cif'%i)
            CifWriter(substrate_set[i]).write_file('substrate_sp_%d.cif'%i)
            CifWriter(hetero_structure[i]).write_file('hetero_sp_%d.cif'%i)
        #print('**********Complete**********')
        return ase_film, ase_substrate, ase_hetero

    def calc_em(self,ase_film,ase_substrate,ase_hetero):
        substrate_energy = []
        film_energy = []
        hetero_energy = []
        match = []
        
        #print('\n********Calculating electrostatic matching*********')
        
        calc_energy(ase_film, film_energy,self._method)
        calc_energy(ase_substrate, substrate_energy,self._method)
        calc_energy(ase_hetero, hetero_energy,self._method)

        #Check electrostatic matching and select the most suitable configuration
        l_list = len(film_energy)
        
        for i in range(l_list):
                #match.append(-(float(film_energy[i])+ float(substrate_energy[i])- float(hetero_energy[i]))/float(hetero_energy[i]))
                match.append(float(film_energy[i]) + float(substrate_energy[i])- float(hetero_energy[i]))
                #A + B - AB
                #print(i,float(film_energy[i]),float(substrate_energy[i]),float(hetero_energy[i]))
        print(match)
        elc_match = match.copy()    
        elc_match.sort()
        final_structure_index = match.index(elc_match[-1])
        result = 'Index of most Electrostatic matching favored structure：%d'%final_structure_index
        return result

