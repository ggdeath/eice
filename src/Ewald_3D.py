'''
author: Wei Shi at 2022
version: 2022.9.19
function: for Ewald3D calculation
'''
import math
import numpy as np
from ase.io import read
from ase import Atoms
from numpy import pi,sqrt,log,exp,cos,sin
from scipy.special import erf
from math import erfc
from numba import jit

@jit(nopython=True)
def _sum_recip_total_energy(kmax, recip_cell, charge1, charge2, distance, alpha):
    sum_addition = 0
    for b0 in range(-kmax,kmax+1):
        for b1 in range(-kmax,kmax+1):
            for b2 in range(-kmax,kmax+1):
                if b0==b1==b2==0:
                    continue
                else:
                    k = b0*recip_cell[0]+b1*recip_cell[1]+b2*recip_cell[2]
                    k2 = np.dot(k,k)
                    structure_factor = charge1*charge2*(np.cos(np.dot(k,distance)))
                    exponential = np.exp(-k2/(4.0*alpha*alpha))/k2
                    addition = exponential*structure_factor
                    sum_addition += addition
    return sum_addition

@jit(nopython=True)
def _sum_real_total_energy(rmax, real_cell, charge1, charge2, distance, alpha):
    sum_addition = 0
    for n0 in range(-rmax,rmax+1):
        for n1 in range(-rmax,rmax+1):
            for n2 in range(-rmax,rmax+1):
                a = n0*real_cell[0]+n1*real_cell[1]+n2*real_cell[2]
                r = distance+a
                dsq = np.dot(r,r)
                if dsq <= 10e-7:
                    continue
                else:
                    d=sqrt(dsq)
                    addition = charge1*charge2*erfc((alpha)*d)/(d)
                    sum_addition += addition
    return sum_addition

@jit(nopython=True)
def _sum_recip_potential(kmax, recip_cell, charge, distance, alpha):
    sum_addition = 0
    for b0 in range(-kmax,kmax+1):
        for b1 in range(-kmax,kmax+1):
            for b2 in range(-kmax,kmax+1):
                if b0==b1==b2==0:
                    continue
                else:
                    k=b0*recip_cell[0]+b1*recip_cell[1]+b2*recip_cell[2]
                    k2 = np.dot(k,k) 
                    structure_factor = charge*(cos(np.dot(k,distance)))
                    exponential = exp(-k2/(4.0*alpha*alpha))/k2 
                    addition = exponential*structure_factor
                    sum_addition += addition
    return sum_addition

@jit(nopython=True)
def _sum_real_potential(rmax, real_cell, charge, distance, alpha, func_p_index):
    sum_addition = 0
    p_switch = 0
    p_index = None
    for n0 in range(-rmax,rmax+1):
        for n1 in range(-rmax,rmax+1):
            for n2 in range(-rmax,rmax+1):
                a = n0*real_cell[0]+n1*real_cell[1]+n2*real_cell[2]
                r = distance+a
                dsq = np.dot(r,r)
                d = sqrt(dsq)
                if dsq <= 10e-7:
                    p_switch = 1
                    p_index = func_p_index
                    continue
                else:
                    addition = charge*erfc((alpha)*d)/d
                    sum_addition += addition
    return sum_addition,p_switch,p_index

def find_chemic_ratio(structure):
    symbols = structure.symbols
    species = {}
    for key in symbols:
        if key in species:
            species[key].append('1')
        else:
            species[key] = ['1']
    for key in species:
            species[key] = len(species[key])
    num_atoms = []
    for key in species:
        num_atoms.append(species[key])
    return species , num_atoms

def findGCD(nums):
        mx, mn = max(nums), min(nums)
        while mx % mn != 0:
            tmp = mx % mn
            mx, mn = mn, tmp
        return mn

class EwaldSummation:
    CONV_FACT=14.399787146105348
    #CONV_FACT=14.39964547058623
    def __init__(self,structure,sp = None,pl = None,rmax=None,kmax=None,alpha=None,):
        self._s=structure
        cell=structure.get_cell()
        length_list=structure.get_cell_lengths_and_angles()
        cell_length=[length_list[0],length_list[1],length_list[2]]
        while 0 in cell_length:
            cell_length.remove(0)
        length=min(cell_length)

        self._vol=structure.get_volume()
        self._alpha= alpha if alpha else (len(structure)* 1/sqrt(2)/(self._vol ** 2))**(1 / 6)*sqrt(pi)
        self._rmax = rmax if rmax else  math.ceil((sqrt(log(10 ** 12))/self._alpha)/length)
        self._kmax = kmax if kmax else  int(2*sqrt(log(10 ** 12))*self._alpha)
        self._sp = sp
        self._pl = pl
        
    def total_energy(self):
        Sum=0
        for i in self._s:
            Sum+=i.charge
        
        if Sum == 0:
            if self._sp:
                total_energy=self._calc_recip()+self._calc_real()+self._calc_epoint()+self._calc_sphe()
            elif self._pl:
                total_energy=self._calc_recip()+self._calc_real()+self._calc_epoint()+self._calc_planar()
            else:
                total_energy=self._calc_recip()+self._calc_real()+self._calc_epoint()
        else:
            if self._sp:
                total_energy=self._calc_recip()+self._calc_real()+self._calc_epoint()+self._calc_sphe()+self._calc_background()
            elif self._pl:
                total_energy=self._calc_recip()+self._calc_real()+self._calc_epoint()+self._calc_planar()+self._calc_background()
            else:
                total_energy=self._calc_recip()+self._calc_real()+self._calc_epoint()+self._calc_background()
        
        return str(total_energy)

    # long_range_term
    def _calc_recip(self):
        Sum=0
        Kcell=self._s.get_reciprocal_cell()*2*pi 
        for i in self._s:
            r1 = i.position
            q1 = i.charge
            for j in self._s:
                r2 = j.position
                q2 = j.charge
                r1r2 = r1-r2
                addition1 = _sum_recip_total_energy(kmax=self._kmax, recip_cell = Kcell, charge1 = q1, charge2 = q2, distance = r1r2, alpha = self._alpha)
                Sum += addition1
                '''
                for b0 in range(-self._kmax,self._kmax+1):
                    for b1 in range(-self._kmax,self._kmax+1):
                        for b2 in range(-self._kmax,self._kmax+1):
                            if b0==b1==b2==0:
                                continue
                            else:
                                k=b0*Kcell[0]+b1*Kcell[1]+b2*Kcell[2]
                                k2 = np.dot(k,k)
                                structure_factor= q1*q2*(np.cos(np.dot(k,r1-r2)))
                                exponential=np.exp(-k2/(4.0*self._alpha*self._alpha))/k2
                                addition =exponential*structure_factor
                                Sum +=addition
                '''
        return EwaldSummation.CONV_FACT*2*pi*Sum/(self._vol)

    # self_term
    def _calc_epoint(self):
        Sum=0
        for i in self._s:
            Sum +=i.charge*i.charge
        return ((-self._alpha)/sqrt(pi))*Sum*EwaldSummation.CONV_FACT

    # short_range_term
    def _calc_real(self):
        Sum = 0
        cell=np.array(self._s.get_cell())
        for i in self._s:
            r1 = i.position
            q1 = i.charge
            for j in self._s:
                r2 = j.position
                q2 = j.charge
                r1r2 = r1-r2
                addition1 = _sum_real_total_energy(rmax=self._rmax, real_cell = cell, charge1 = q1, charge2 = q2, distance = r1r2, alpha = self._alpha)
                '''
                for n0 in range(-self._rmax,self._rmax+1):
                    for n1 in range(-self._rmax,self._rmax+1):
                        for n2 in range(-self._rmax,self._rmax+1):
                            a=n0*cell[0]+n1*cell[1]+n2*cell[2]
                            r=r1-r2+a
                            dsq=np.dot(r,r)
                            if dsq<=10e-7:
                                continue
                            else:
                                d=sqrt(dsq)
                                addition = q1*q2*erfc((self._alpha)*d)/(d)
                                Sum += addition
                '''
                Sum += addition1
        return Sum*EwaldSummation.CONV_FACT*1/2

    # planar infinite boundary term
    def _calc_planar(self):
        Sum=0
        cell=self._s.get_cell()
        for i in self._s:
            r1 = i.position[2]
            q1 = i.charge
            for j in self._s:
                r2 = j.position[2]
                q2 = j.charge
                zij=r1-r2
                dsq=zij*zij
                addition=q1*q2*dsq
                Sum+=addition
        return -EwaldSummation.CONV_FACT*pi*Sum/(self._vol)

    # spherical infinite boundary term
    def _calc_sphe(self):
        Sum=0
        cell=self._s.get_cell()
        for i in self._s:
            r1 = i.position[2]
            q1 = i.charge
            for j in self._s:
                r2 = j.position[2]
                q2 = j.charge
                zij=r1-r2
                dsq=zij*zij
                addition=q1*q2*dsq
                Sum+=addition
        return -EwaldSummation.CONV_FACT*pi*Sum/(self._vol*3)

    def _calc_background(self):
        Sum=0
        for i in self._s:
            Sum +=i.charge
        return -EwaldSummation.CONV_FACT*(pi*(Sum**2))/(2*self._alpha*self._alpha*self._vol)

    def get_madelung_potential(self,position=None,scaled=1):
               
        Kcell=self._s.get_reciprocal_cell()*2*pi
        cell = np.array(self._s.get_cell())
        r_1 = np.array(position)

        if scaled:
            r1 = np.dot(r_1 , cell)
        else:
            r1 = r_1
            
        Sum=0
        switch=0
        ####################### longe range ####################
        ''''
        for b0 in range(-self._kmax,self._kmax+1):
            for b1 in range(-self._kmax,self._kmax+1):
                for b2 in range(-self._kmax,self._kmax+1):
                    if b0==b1==b2==0:
                        continue
                    else:
                        k=b0*Kcell[0]+b1*Kcell[1]+b2*Kcell[2]
                        k2 = np.dot(k,k)
        '''
        for i in self._s:
            r2 = i.position.copy()
            q2 = i.charge.copy()
            r1r2 = r1-r2
            #print(Kcell)
            addition1 = _sum_recip_potential(kmax=self._kmax, recip_cell = Kcell, charge = q2, distance = r1r2, alpha = self._alpha)
            Sum += addition1
                        
        recip_potential=EwaldSummation.CONV_FACT*4*pi*Sum/(self._vol)
        #print("recip_potential",recip_potential)

        ####################### short range ####################
        Sum = 0
        self_potential = 0
        #cell=np.array(self._s.get_cell())
        for i in self._s:
            r2 = i.position.copy()
            r1r2 = r1-r2
            q2 = i.charge
            particle_index = i.index
            '''
            for n0 in range(-self._rmax,self._rmax+1):
                for n1 in range(-self._rmax,self._rmax+1):
                    for n2 in range(-self._rmax,self._rmax+1):
                        a=n0*cell[0]+n1*cell[1]+n2*cell[2]
                        r=r1-r2+a
                        dsq=np.dot(r,r)
                        d=sqrt(dsq)
                        if dsq<=10e-7:
                            index=i.index
                            switch=1
                            continue
                        else:
                            addition = i.charge.copy()*erfc((self._alpha)*d)/d
                            Sum += addition
            '''
            addition1, switch, index = _sum_real_potential(rmax=self._rmax, real_cell = cell, charge = q2, distance = r1r2, alpha = self._alpha, func_p_index = particle_index)
            if switch:
                self_potential=EwaldSummation.CONV_FACT*2*self._alpha*self._s[index].charge/sqrt(pi)
            Sum += addition1
        real_potential= Sum*EwaldSummation.CONV_FACT
        #print("real_potential",real_potential)

        ############################################################ 
        
        potential=(recip_potential+real_potential-self_potential)
        #print("self_potential",self_potential)

        Sum = 0
        for i in self._s:
            r2=i.position.copy()
            r=r1[2]-r2[2]
            dsq=r*r
            Sum+=i.charge.copy()*dsq
        pl_potential = -Sum*2*EwaldSummation.CONV_FACT*pi/(self._vol)

        Sum = 0
        for i in self._s:
            r2=i.position.copy()
            r=r1-r2
            dsq=np.dot(r,r)
            Sum+=i.charge.copy()*dsq
        sp_potential = -Sum*2*EwaldSummation.CONV_FACT*pi/(self._vol*3)

        if self._sp:
            potential = potential + sp_potential
        elif self._pl:
            potential = potential + pl_potential
        else:
            potential = potential
        
        return str(potential)
    
    def get_madelung_constant(self):
        result = ''
        atom_name_list = []
        atom_index_list = []
        mad_cons = []
        madelung_ion_nn = {}
        madelung_ion_a0 = {}
        ion_charge = {}
        madelung_nn = 0
        madelung_a0 = 0
        species , num_atoms = find_chemic_ratio(self._s)
        min_num = findGCD(num_atoms)
        d = self._s.get_cell_lengths_and_angles ()
        a0 = (self._vol)**(1/3)

        
        for key in species:
            species[key] = species[key]/min_num
        
        for i in self._s:
            atom_name_list.append(i.symbol)
        atom_name_list = list(set(atom_name_list))
        for i in atom_name_list:
            for j in self._s:
                if j.symbol == i:
                    atom_index_list.append(j.index)
                    break
                else:
                    continue

        for i in atom_index_list:
            Sum=0
            switch=0
            r1 = np.array(self._s[i].position)
            
            Kcell=self._s.get_reciprocal_cell()*2*pi 
            for b0 in range(-self._kmax,self._kmax+1):
                for b1 in range(-self._kmax,self._kmax+1):
                    for b2 in range(-self._kmax,self._kmax+1):
                        if b0==b1==b2==0:
                            continue
                        else:
                            k=b0*Kcell[0]+b1*Kcell[1]+b2*Kcell[2]
                            k2 = np.dot(k,k) 
                            for i in self._s:
                                    r2 = i.position.copy()
                                    q2 = i.charge
                                    structure_factor= q2*(cos(np.dot(k,r1-r2)))
                                    exponential=exp(-k2/(4.0*self._alpha*self._alpha))/k2
                                    addition =exponential*structure_factor
                                    Sum +=addition    
            recip_potential=EwaldSummation.CONV_FACT*4*pi*Sum/(self._vol)
       
            Sum = 0
            cell=self._s.get_cell()
            for i in self._s:
                r2=i.position.copy()
                for n0 in range(-self._rmax,self._rmax+1):
                    for n1 in range(-self._rmax,self._rmax+1):
                        for n2 in range(-self._rmax,self._rmax+1):
                            a=n0*cell[0]+n1*cell[1]+n2*cell[2]
                            r=r1-r2+a
                            dsq=np.dot(r,r)
                            d=sqrt(dsq)
                            if dsq<=10e-7:
                                index=i.index
                                switch=1
                                continue
                            else:
                                addition = i.charge*erfc((self._alpha)*d)/d
                                Sum += addition
            real_potential= Sum*EwaldSummation.CONV_FACT
       
            if switch:
                
                self_potential=EwaldSummation.CONV_FACT*2*self._alpha*self._s[index].charge/sqrt(pi)
                potential=(recip_potential+real_potential-self_potential)
                distances = np.around((self._s.get_all_distances(mic=True)), decimals=8)
                distance=list(distances[index])
                d_list=distance.copy()
                distance.remove(0)
                nn=min(distance)
                nn_index=0
                
                for i,d in enumerate(d_list):
                    if nn==d:
                        nn_index=i
                    else:
                        continue
                    
                madelung_ion_nn[self._s[index].symbol] = ((potential)*nn/EwaldSummation.CONV_FACT)
                madelung_ion_a0[self._s[index].symbol] = ((potential)*a0/EwaldSummation.CONV_FACT)
                ion_charge[self._s[index].symbol] = self._s[index].charge
                result = result + (self._s[index].symbol + ': ' + str(madelung_ion_nn[self._s[index].symbol])) + '\n'
            else:
                print('wrong')

        for key in species:
               mn = abs(madelung_ion_nn[key]) * abs(ion_charge[key]) * species[key]
               ma = abs(madelung_ion_a0[key]) * abs(ion_charge[key]) * species[key]
               
               madelung_nn += mn
               madelung_a0 = madelung_a0 + ma

        madelung_nn = madelung_nn/2
        madelung_a0 = madelung_a0/2
       # print(ion_charge,species)

        result += 'Mr: ' + str(madelung_nn) + '\n'
        result += 'Ma: ' + str(madelung_a0) + '\n'
        result += 'nn: ' + str(nn) + '\n'
        result += 'a0: ' + str(a0) + '\n'
        #result += 'ma'   + str()
       
        return result
           
    def force(self):
        result = ''
        for ion in self._s:
            Sum=0
            r1 = ion.position
            Kcell=self._s.get_reciprocal_cell()*2*pi
            for i in self._s:
                r2 = i.position.copy()
                q2 = i.charge.copy()
                for b0 in range(-self._kmax,self._kmax+1):
                    for b1 in range(-self._kmax,self._kmax+1):
                        for b2 in range(-self._kmax,self._kmax+1):
                            if b0==b1==b2==0:
                                continue
                            else:
                                k=b0*Kcell[0]+b1*Kcell[1]+b2*Kcell[2]
                                k2 = np.dot(k,k) 
                                structure_factor= q2*(sin(np.dot(k,r1-r2)))
                                exponential=exp(-k2/(4.0*self._alpha*self._alpha))/k2 
                                addition =exponential*structure_factor*k
                                Sum +=addition            
            recip=EwaldSummation.CONV_FACT*4*pi*Sum/(self._vol) 

            Sum = 0
            cell=self._s.get_cell()
            for i in self._s:
                r2=i.position
                for n0 in range(-self._rmax,self._rmax+1):
                    for n1 in range(-self._rmax,self._rmax+1):
                        for n2 in range(-self._rmax,self._rmax+1):
                            a=n0*cell[0]+n1*cell[1]+n2*cell[2]
                            r=r1-r2+a
                            dsq=np.dot(r,r)
                            d=sqrt(dsq)
                            if dsq<=10e-7:
                                continue
                            else:
                                part1=(2*self._alpha/sqrt(pi))*exp(-(self._alpha*self._alpha)*dsq)/dsq
                                part2=erfc(self._alpha*d)/(d**3)
                                addition= i.charge*(part1+part2)*r
                                Sum+=addition
            real= Sum*EwaldSummation.CONV_FACT

            Sum = 0
            for j in self._s:
                zj = j.position[2]
                q2 = j.charge
                addition=q2*zj
                Sum+=addition
            pl = -EwaldSummation.CONV_FACT*4*pi*Sum/(self._vol)

            for j in self._s:
                q2 = j.charge
                addition=q2*j.position
                Sum+=addition
            sp = -EwaldSummation.CONV_FACT*4*pi*Sum/(self._vol*3)
       
            if self._pl:
                F=ion.charge*(recip+real+np.array([0,0,pl]))
            elif self._sp:
                F=ion.charge*(recip+real+sp)
            else:
                F=ion.charge*(recip+real)
          
            result = result + ion.symbol + str(ion.index)+' ' + str(F)
            result = result + '\n'
        return(result)
