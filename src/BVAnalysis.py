'''
author: hebing at 2017
        Wei Shi at 2022
version: 2021.12.5
function: for bond valence method analysis
'''
import os
import numpy as np
from scipy.special import erfc 
from scipy.ndimage import measurements
import scipy.ndimage
import math
from struct import pack
from ewald_cation import Ewald_cation_summation

def loadbvparam(filename):
    with open(filename,'r') as fp:
        lines=fp.readlines()
    bvmpara={}
    for line in lines:
        varstr=line.split('\t')
        key=varstr[0]+varstr[1]+varstr[2]+varstr[3]
        if key in bvmpara:
            bvmpara[key].append([float(varstr[4]),float(varstr[5])])
        else:
            bvmpara[key]=[[float(varstr[4]),float(varstr[5])]]
    return bvmpara

def loadElementsparam(filename):
    with open(filename,'r') as fp:
        lines=fp.readlines()
    Elementsparam={}
    for line in lines:
        line=line.strip('\n')
        varstr=line.split('\t')
        key=varstr[1]+varstr[2]
        Elementsparam[key]=[float(varstr[0]),float(varstr[3]),float(varstr[4]),float(varstr[5]),float(varstr[6]),float(varstr[7]),float(varstr[8]),float(varstr[9])]
    return Elementsparam    

class BVAnalysis(object):
    '''
    for bond valence method analyze ion channel
    '''
    def __init__(self, struc=None,ewaldstructure=None,nnn= None, size=[50,50,50]):
        self._Struct=struc
        self._ewaldstructure=ewaldstructure
        self._Size=[50,50,50]
        self.__Data={}
        self.__Max={}
        self.__Min={}
        self.Ea={}
        self._MoveIon='Li'
        self._nnn = nnn
        self._poss=None
        self.ValenceOfMoveIon=1
        self.stop=False
        module_dir = os.path.dirname(os.path.abspath(__file__))

        self._BVparam= loadbvparam(os.path.join(module_dir, 'bvmparam.dat'))
        self._Elementsparam=loadElementsparam(os.path.join(module_dir, 'elements.dat'))

    def get_data(self):
        return self.__Data


    def get_max(self):
        return self.__Max


    def get_min(self):
        return self.__Min


    def set_data(self, value):
        self.__Data = value


    def set_max(self, value):
        self.__Max = value


    def set_min(self, value):
        self.__Min = value


    def del_data(self):
        del self.__Data


    def del_max(self):
        del self.__Max


    def del_min(self):
        del self.__Min


    @property
    def BVSMin(self):
        return self.__Min['BVS']
    @property
    def BVSMax(self):
        return self.__Max['BVS']
    
    def SetStructure(self,struc):
        self._Struct=struc
    def Setewaldstructure(self,ewaldstructure):
        self._ewaldstructure=ewaldstructure
    def Setpoints_nnn_remove(self):
        self._nnn=1

    def SetMoveIon(self,moveion):
        self._MoveIon=moveion
   
    def SetResolution(self,size):
        self._Size=size
    def SetLengthResolution(self,reslen=0.1):
        abc=self._Struct.Getabc()
        self._reslen=reslen
        self._Size[0]=int(abc[0]/reslen)+1
        self._Size[1]=int(abc[1]/reslen)+1
        self._Size[2]=int(abc[2]/reslen)+1
    
    def SaveBVEWData(self,filename):
        self.SaveBinaryData(filename+'_BVEW.pgrid', self.__Data['BVEW'],'BVEW')
    def SaveData(self,filename,data,datatype=' '):
        fp=open(filename,'w')
        fp.write(datatype+'\n')
        alpha=self._Struct.GetAlphaBetaGama()
        abc=self._Struct.Getabc()
        fp.write(str(abc[0])+' '+str(abc[1])+' '+str(abc[2])+' ')
        fp.write(str(alpha[0])+' '+str(alpha[1])+' '+str(alpha[2])+'\n')
        fp.write(str(self._Size[0])+' '+str(self._Size[1])+' '+str(self._Size[2])+'\n ')
        for i in range(self._Size[0]):
            for j in range(self._Size[1]):
                for k in range(self._Size[2]):
                    fp.write(str(data[i][j][k])+'\n')
        fp.close()
    def SaveBinaryData(self,filename,data,datatype=' '):
        latticeparam=self._Struct.Getabc()+self._Struct.GetAlphaBetaGama()
        with open(filename, 'wb') as outf:
            outf.write(pack('4i', 3, 0, 0, 0))  # file format version
            outf.write(pack('80s', datatype.ljust(80).encode('utf-8')))  # Title 80 characters
            outf.write(pack('i', 1))  # gType 0 for *.ggrid, 1 for *.pgrid
            outf.write(pack('i', 0))  # fType 0
            outf.write(pack('i', 1))  # nVal dummy
            outf.write(pack('i', 3))  # dimension
            outf.write(pack('3i', *self._Size))  # numbers of voxels
            outf.write(pack('i', data.size))  # Total number of voxels
            outf.write(pack('6f', *latticeparam))  # lattice parameters
            for k in range(self._Size[2]):
                for j in range(self._Size[1]):
                    for i in range(self._Size[0]):
                        outf.write(pack('f', data[i][j][k]))
    def ReadData(self,filename,data,datatype=' '):
        fp=open(filename,'r')
        datatype=fp.readline()
        alpha=self._Struct.GetAlphaBetaGama()
        abc=self._Struct.Getabc()
        abc=fp.readline().split()
        alpha=fp.readline().split()
        strs=fp.readline().split()
        size=[ int(i) for i in strs]
        if datatype=='bvs':
            self.__Data['BVS']=np.zeros(size)
            data=self.__Data['BVS']
        else:
            self.__Data['BVSE']=np.zeros(size)
            data=self.__Data['BVSE']
        for i in range(size[2]):
            for j in range(size[1]):
                for k in range(size[0]):
                    strs=fp.readline().split()
                    data[k][j][i]=int(strs[0])
                 
        fp.close()
    def GetBVEWData(self):
        return self.__Data['BVEW']
    def GetPosSet(self):
        return self._poss
    def CaluGlobalIndex(self,):
        self.stop=False
        self._Size.reverse()
        centrepos=np.zeros(self._Size+[3])
        self._Data=np.zeros(self._Size,dtype=np.double)
        for k in range(self._Size[0]):
            for j in range(self._Size[1]):
                for i in range(self._Size[2]):
                    centrepos[k][j][i][2]=k/(self._Size[0]-1.0)
                    centrepos[k][j][i][1]=j/(self._Size[1]-1.0)
                    centrepos[k][j][i][0]=i/(self._Size[2]-1.0)
 
        self._poss=self._Struct.FracPosToCartPos(centrepos)
        (distance,neighborsindex)=self._Struct.GetKNeighbors(self._poss, kn=8)
              
        site2atoms=None
        for k in range(self._Size[0]):
            for j in range(self._Size[1]):
                for i in range(self._Size[2]):
                    if self.stop:
                        return 
                    for dindex,index in enumerate(neighborsindex[k][j][i]):
                        site2=self._Struct.GetSuperCellusites()[index]
                        if site2.GetIronType()*self.ValenceOfMoveIon<0:
                            for key in site2.GetElementsOccupy():
                                if key!='Vac':
                                    site2atoms=key
                            if self.ValenceOfMoveIon>0:
                                key="".join([self._MoveIon,str(self.ValenceOfMoveIon),site2atoms,str(site2.GetElementsOxiValue()[site2atoms])])
                            else:
                                key="".join([site2atoms,str(site2.GetElementsOxiValue()[site2atoms]),self._MoveIon,str(self.ValenceOfMoveIon)])
                            if key in self._BVparam:
                                (r,b)=self._BVparam[key][0]
                                bv=np.exp((r-distance[k][j][i][dindex])/b)
                                self.__Data['BVS'][k][j][i]=self.__Data['BVS'][k][j][i]+bv
                            else:
                                print('Not Find bvs param'+key)
                        else:
                            pass
                            #print(site2.GetSiteLabel())                            
    def CaluBVEW(self,ProgressCall):
        ewald=Ewald_cation_summation(self._ewaldstructure)
        #print(ewald._alpha,ewald._rmax,ewald._kmax)
        ewald.setMoveIon(self._MoveIon)
        ewald.set_vor_points()
        ewald.migion_charge = self.ValenceOfMoveIon
        ewald.moveion_ori_index()
        if len(self._Struct._OxidationTable)==0: 
            raise Exception("can't caculation bvs and Ea without atom oxi info !")
        self.stop=False
        centrepos=np.zeros(self._Size+[3])
        self.__Data['BVEW']=np.zeros(self._Size,dtype=np.double)
        for k in range(self._Size[2]):
            for j in range(self._Size[1]):
                for i in range(self._Size[0]):
                    centrepos[i][j][k][2]=k/(self._Size[2]-1.0)
                    centrepos[i][j][k][1]=j/(self._Size[1]-1.0)
                    centrepos[i][j][k][0]=i/(self._Size[0]-1.0)
        if ProgressCall:
            ProgressCall(10)
        self._poss=self._Struct.FracPosToCartPos(centrepos)
      
                        
        atomsq={}

        self._ewaldstructure.append('Li')
        self._ewaldstructure[-1].charge = 1
        
        for i in range(self._Size[0]):            
            (distance,neighborsindex)=self._Struct.GetKNeighbors(self._poss[i], kn=128)
            if ProgressCall:
                ProgressCall(10+i*90/(self._Size[0]-1))
            for j in range(self._Size[1]):
                for k in range(self._Size[2]):
                    self._ewaldstructure[-1].position = self._poss[i][j][k]
                    if self._nnn:
                        ewald_energy = ewald.single_energy_remove_nnn(self._poss[i][j][k])
                    else:
                        ewald_energy = ewald.single_energy(self._poss[i][j][k])
                    if self.stop:
                        return False
                    bvsdata=0.0
                    data=0.0
                    cdata=0.0
                    N=0
                    D0=0.0
                    Rcutoff=10.0
                    alpha=0.0
                    occupyvalue=0.0
                    for dindex,index in enumerate(neighborsindex[j][k]):
                        site2=self._Struct._SuperCellusites[index]
                        if site2.GetIronType()*self.ValenceOfMoveIon<0:
                            for (ssymbol,occupyvalue) in site2.GetElementsOccupy().items():
                                if ssymbol!='Vac':
                                    site2oxi=site2.GetElementsOxiValue()[ssymbol]
                                    if self.ValenceOfMoveIon>0:
                                        key="".join([self._MoveIon,str(self.ValenceOfMoveIon),ssymbol,str(site2oxi)])
                                    else:
                                        key="".join([ssymbol,str(site2oxi),self._MoveIon,str(self.ValenceOfMoveIon)])
                                    if  key in self._BVparam :
                                        (r,b)=self._BVparam[key][0]
                                        Rcutoff=10.0
                                        if distance[j][k][dindex]<=Rcutoff:
                                            bvs=np.exp((r-distance[j][k][dindex])/b)
                                            bvsdata=bvsdata+bvs 
                    self.__Data['BVEW'][i][j][k]=abs(ewald_energy)/(abs(bvsdata-1)**10 + 1)
        self.__Data['BVEW']=self.__Data['BVEW']
