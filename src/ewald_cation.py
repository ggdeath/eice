'''
author: Wei Shi at 2022
version: 2022.4.7
function: Madelung potential for BVEW
'''
import math
import numpy as np
from ase.io import read
from ase import Atoms
from numpy import pi,sqrt,log,exp,cos,sin
from scipy.special import erf
from math import erfc
from scipy.spatial import Voronoi
from scipy.spatial import cKDTree
from numba import jit

@jit(nopython=True)
def _sum_recip(kmax, recip_cell, charge, distance, alpha):
    sum_addition = 0
    for b0 in range(-kmax,kmax+1):
        for b1 in range(-kmax,kmax+1):
            for b2 in range(-kmax,kmax+1):
                if b0==b1==b2==0:
                    continue
                else:
                    k=b0*recip_cell[0]+b1*recip_cell[1]+b2*recip_cell[2]
                    k2 = np.dot(k,k) 
                    structure_factor = charge*(cos(np.dot(k,distance)))
                    exponential = exp(-k2/(4.0*alpha*alpha))/k2 
                    addition = exponential*structure_factor
                    sum_addition += addition
    return sum_addition

@jit(nopython=True)
def _sum_real(rmax, real_cell, charge, distance, alpha, func_p_index):
    sum_addition = 0
    p_switch = 0
    p_index = None
    for n0 in range(-rmax,rmax+1):
        for n1 in range(-rmax,rmax+1):
            for n2 in range(-rmax,rmax+1):
                a = n0*real_cell[0]+n1*real_cell[1]+n2*real_cell[2]
                r = distance+a
                dsq = np.dot(r,r)
                d = sqrt(dsq)
                if dsq <= 10e-7:
                    p_switch = 1
                    p_index = func_p_index
                    continue
                else:
                    addition = charge*erfc((alpha)*d)/d
                    sum_addition += addition
    return sum_addition,p_switch,p_index

class Ewald_cation_summation:
    CONV_FACT=14.39964547058623
    def __init__(self,structure,rmax=None,kmax=None,alpha=None):
        self._s=structure
        cell=structure.get_cell()
        length_list=structure.get_cell_lengths_and_angles()
        cell_length=[length_list[0],length_list[1],length_list[2]]
        while 0 in cell_length:
            cell_length.remove(0)
        length=min(cell_length)
        self._MoveIon='Li'
        self._migion_charge = 1
        self._vol=structure.get_volume()
        self._alpha= alpha if alpha else (len(structure)* 1/sqrt(2)/(self._vol ** 2))**(1 / 6)*sqrt(pi)
        self._rmax = rmax if rmax else  math.ceil((sqrt(log(10 ** 12))/self._alpha)/length)
        self._kmax = kmax if kmax else  int(2*sqrt(log(10 ** 12))*self._alpha)
        self._charges=structure.get_initial_charges()
        self._cell=structure.get_cell()
        self._list_of_Li = []
        self._voronoi_kdtree = []
        self._vor_switch = 0
        self._moveion_index = 0
        self._move_pre_list = []

    @property    
    def cutoff(self):
        print("rmax,kmax:",self._rmax,self._kmax)

    def set_vor_points(self):
        cell=self._s.get_cell()
        vor_points = []
        move_list = []
        sum1 = 0
        for i in self._s:
            if i.symbol == '%s'%(self._MoveIon):
                self._move_pre_list.append(sum1)
                for n0 in range(-1,2):
                    for n1 in range(-1,2):
                        for n2 in range(-1,2):
                            sum1 += 1
                            a = n0*cell[0]+n1*cell[1]+n2*cell[2]
                            p = i.position + a
                            vor_points.append(p)
            else:
                continue
       
        if vor_points:
            vor_points = np.array(vor_points)
            self._voronoi_kdtree = cKDTree(vor_points)
            self._vor_switch = 1
        else:
            self._vor_switch = 0

    def moveion_ori_index(self):
        for i in self._s:
           if i.symbol == '%s'%(self._MoveIon):
               moveion_index = i.index
               self._moveion_index = moveion_index
               break
           else:
               continue
            
    def setMoveIon(self,moveion):
        self._MoveIon=moveion

    def single_energy(self,position=None):
        r1 = np.array(position)
        ion_remove = 2000
        
        if self._vor_switch:
           test_point_dist, test_point_regions = self._voronoi_kdtree.query(position)
           for test_point,j in enumerate(self._move_pre_list):
               if j <= test_point_regions < j+27 :
                   break
               else:
                   continue
           ion_remove = test_point + self._moveion_index
        else:
            print('wrong')

        ########################### long_range_term ##########################

        Sum=0
        switch=0
        Kcell=self._s.get_reciprocal_cell()*2*pi
        for i in self._s:
            r2 = i.position
            q2 = i.charge
            r1r2 = r1-r2
            if q2 <= 0 :
                continue
            elif i.index == ion_remove :
                continue
            else:
                addition1 = _sum_recip(kmax=self._kmax, recip_cell = Kcell, charge = q2, distance = r1r2, alpha = self._alpha)
                Sum += addition1
        
        recip_potential=4*pi*Sum*Ewald_cation_summation.CONV_FACT/(self._vol) 
        #print('recip_potential',recip_potential)
        
        ######################## short_range_term ##############################

        Sum = 0
        cell=np.array(self._s.get_cell())
        for i in self._s:
            r2 = i.position
            r1r2 = r1-r2
            q2 = i.charge
            particle_index = i.index
            if q2 <=0:
                continue
            elif i.index == ion_remove :
                continue
            else:
                addition1, switch, index = _sum_real(rmax=self._rmax, real_cell = cell, charge = q2, distance = r1r2, alpha = self._alpha, func_p_index = particle_index)
                #print(addition1)
                Sum += addition1
                
        real_potential = Sum*Ewald_cation_summation.CONV_FACT
        #print('real_potential',real_potential)

        ######################### net_charge ##############################
        net_charge=0
        for i in self._s:
            if i.index == ion_remove :
                continue
            elif i.charge < 0:
                continue
            else:
                net_charge += i.charge
            
        self_potential=0
        migion_charge = self._migion_charge

        if switch :
            self_potential=2*self._alpha*self._s[index].charge*Ewald_cation_summation.CONV_FACT/sqrt(pi)
            single_energy=(recip_potential+real_potential-self_potential)*self._s[index].charge
            back_energy = -self._s[index].charge*net_charge*pi*Ewald_cation_summation.CONV_FACT/(self._alpha*self._alpha*self._vol)
            #back_energy = -(net_charge**2)*pi*EwaldSummation.CONV_FACT/(2*self._alpha*self._alpha*self._vol)
        else :
            single_energy=(recip_potential+real_potential)*migion_charge
            back_energy = -migion_charge*Ewald_cation_summation.CONV_FACT*net_charge*pi/(self._alpha*self._alpha*self._vol)
            #back_energy = -(net_charge**2)*pi*EwaldSummation.CONV_FACT/(2*self._alpha*self._alpha*self._vol)

        ######################### total_energy ##############################
        total_energy = single_energy+back_energy

        if total_energy>0:
            return 0
        else:
            return total_energy

    def single_energy_remove_nnn(self,position=None):
        r1 = np.array(position)
        ion_remove = 2000
        
        if self._vor_switch:
           test_point_dist, test_point_regions = self._voronoi_kdtree.query(position,k=3)
           test_point_region1 = test_point_regions[0]
           test_point_region2 = test_point_regions[1]
           test_point_region3 = test_point_regions[2]
           
           for test_point,j in enumerate(self._move_pre_list):
               if j <= test_point_region1 < j+27 :
                   break
               else:
                   continue
           ion_remove1 = test_point + self._moveion_index

           for test_point,j in enumerate(self._move_pre_list):
               if j <= test_point_region2 < j+27 :
                   break
               else:
                   continue
           ion_remove2 = test_point + self._moveion_index

           if ion_remove1 == ion_remove2:
               for test_point,j in enumerate(self._move_pre_list):
                   if j <= test_point_region3 < j+27 :
                       break
                   else:
                       continue
           ion_remove2 = test_point + self._moveion_index
        else:
            print('wrong')

           #if ion_remove1 == ion_remove2:
           #    print(ion_remove1,ion_remove2)
               
        ########################### long_range_term ##########################

        Sum=0
        switch=0
        Kcell=self._s.get_reciprocal_cell()*2*pi
        for i in self._s:
            r2 = i.position
            q2 = i.charge
            r1r2 = r1-r2
            if q2 <= 0 :
                continue
            elif i.index == ion_remove1 :
                continue
            elif i.index == ion_remove2 :
                continue
            else:
                addition1 = _sum_recip(kmax=self._kmax, recip_cell = Kcell, charge = q2, distance = r1r2, alpha = self._alpha)
                Sum += addition1
        
        recip_potential=4*pi*Sum*Ewald_cation_summation.CONV_FACT/(self._vol) 
        #print('recip_potential',recip_potential)
        
        ######################## short_range_term ##############################

        Sum = 0
        cell=np.array(self._s.get_cell())
        for i in self._s:
            r2 = i.position
            r1r2 = r1-r2
            q2 = i.charge
            particle_index = i.index
            if q2 <=0:
                continue
            elif i.index == ion_remove1 :
                continue
            elif i.index == ion_remove2 :
                continue
            else:
                addition1, switch, index = _sum_real(rmax=self._rmax, real_cell = cell, charge = q2, distance = r1r2, alpha = self._alpha, func_p_index = particle_index)
                #print(addition1)
                Sum += addition1
                
        real_potential = Sum*Ewald_cation_summation.CONV_FACT
        #print('real_potential',real_potential)

        ######################### net_charge ##############################
        net_charge=0
        for i in self._s:
            if i.index == ion_remove1 :
                continue
            elif i.index == ion_remove2 :
                continue
            elif i.charge < 0:
                continue
            else:
                net_charge += i.charge
            
        self_potential=0
        migion_charge = self._migion_charge

        if switch :
            self_potential=2*self._alpha*self._s[index].charge*Ewald_cation_summation.CONV_FACT/sqrt(pi)
            single_energy=(recip_potential+real_potential-self_potential)*self._s[index].charge
            back_energy = -self._s[index].charge*net_charge*pi*Ewald_cation_summation.CONV_FACT/(self._alpha*self._alpha*self._vol)
            #back_energy = -(net_charge**2)*pi*EwaldSummation.CONV_FACT/(2*self._alpha*self._alpha*self._vol)
        else :
            single_energy=(recip_potential+real_potential)*migion_charge
            back_energy = -migion_charge*Ewald_cation_summation.CONV_FACT*net_charge*pi/(self._alpha*self._alpha*self._vol)

        ######################### total_energy ##############################
        total_energy = single_energy+back_energy
        if total_energy>0:
            return 0
        else:
            return total_energy
