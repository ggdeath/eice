'''
author: Wei Shi at 2022
function: for BVEW init
'''
import ase.io
import os
import Structure
import BVAnalysis

class Bondvalence_ewald:
    def __init__(self, filename, ewaldstructure, moveion='Li', valenceofmoveion=1, resolution=0.2, rm_nnn = 0):
        self._filename = filename
        self._ewaldstructure = ewaldstructure
        self._moveion = moveion
        self._valenceofmoveion = valenceofmoveion
        self._resolution = resolution
        self._rm_nnn = rm_nnn
        
    def bv_calculation(self):
        atoms = ase.io.read(self._filename, store_tags=True)
        struc = Structure.Structure()
        struc.GetAseStructure(atoms)
        
        bvs = BVAnalysis.BVAnalysis()
        bvs.SetStructure(struc)
        bvs.Setewaldstructure(self._ewaldstructure)
        bvs.SetMoveIon(self._moveion)
        if self._rm_nnn :
            bvs.Setpoints_nnn_remove()
        bvs.ValenceOfMoveIon = self._valenceofmoveion
        bvs.SetLengthResolution(self._resolution)
        bvs.CaluBVEW(None)
        bv_data = bvs.get_data()
        bvs.SaveBVEWData(os.path.splitext(self._filename)[0])
        da=bvs.GetBVEWData()
